/**
 * AngularJS factory for lists in ToDo app
 * Author: Stephen Bouckaert (stephenbouckaert.com)
 * Created: 12/12/2014
 */
(function() {
    var taskFactory = function($resource, $cacheFactory){
        return $resource('/api/v1/tasks/:id',
        {},
        // Query parameters
		{
			'query': {
				method: 'GET',
				cache: true
				},
			'update': {
					method: 'PUT',
					params: {task: '@task', completed: '@completed'}, 
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    			}
		}
    );
    };
    taskFactory.$inject = ['$resource', '$cacheFactory'];
    angular.module('todoApp')
        .factory('taskFactory', taskFactory);
}());