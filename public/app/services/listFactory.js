/**
 * AngularJS factory for lists in ToDo app
 * Author: Stephen Bouckaert (stephenbouckaert.com)
 * Created: 11/12/2014
 */
(function() {
    var listFactory = function($resource, $cacheFactory){
        return $resource('/api/v1/lists/:id',
        {},
        // Query parameters
		{
			'query': {
				method: 'GET',
				cache: true
				},
			'update': {
					method: 'PUT',
					params: {data: '@data'}, 
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    			}
		}
    );
    };
    listFactory.$inject = ['$resource', '$cacheFactory'];
    angular.module('todoApp')
        .factory('listFactory', listFactory);
}());