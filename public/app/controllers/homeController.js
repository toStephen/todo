/**
 * AngularJS controller for ToDo app
 * Author: Stephen Bouckaert (stephenbouckaert.com)
 * Created: 10/12/2014
  */
  
(function(){
var HomeController = function($scope, listFactory, taskFactory, $cacheFactory, $log) {
    // Variable declarations
	$scope.lists = [];
	$scope.defaultAddNewList = '';
	$scope.defaultAddNewListNotValid = false;
	$scope.defaultAddNewTask = '';
	$scope.defaultAddNewTaskNotValid = false;
	$scope.activeListId = '';
	$scope.activeList = [];
	$scope.listCount = Object.keys($scope.lists).length;
	
    // Initialise the controller
    function init(){
	    // Get all the lists
	    // @return result JSON
		listFactory.get(
		function(data){
			// Fill global variables with retrieved data
			$scope.lists = data.lists;
			$scope.activeListId = Object.keys(data.lists)[0];
			$scope.activeList = data.lists[$scope.activeListId];
			$scope.listCount = Object.keys($scope.lists).length;
		},
		function(){
			// Error
		});
    }
    
    // Remove a list
    // @param list OBJECT
	// @return void
    $scope.removeList = function(list){
	    listFactory.remove({id:list.id},
		function(data){
			// Success
			delete $scope.lists[list.id];
			$scope.listCount = Object.keys($scope.lists).length;
			$scope.activeListId = Object.keys($scope.lists)[0];
			$scope.activeList = $scope.lists[$scope.activeListId];
		},
		function(){
			// Error
			$log.info('Error on deleting list');
		});
    }
    
    // Update a list
    // @param1 list OBJECT
    // @param2 data OBJECT
	// @return void
    $scope.updateList = function(list, fdata){
	    listFactory.update({id:list.id}, {data:fdata},
		function(data){
			// Success
			$scope.lists[list.id].name = fdata;
		},
		function(){
			// Error
			$log.info('Error on updating list');
		});
    }
    
    // Add a list 
    // @param text STRING
    // @return result JSON
    $scope.addList = function(fdata){
	    var list = new listFactory();
		list.name = fdata.target.value;
		list.$save(
		function(listRes){
			// Success
			$scope.defaultAddNewListNotValid = false;
			$scope.defaultAddNewList = '';
			$scope.lists[listRes.id] = listRes;
			$scope.listCount = Object.keys($scope.lists).length;
		},
		function(){
			// Error
			$scope.defaultAddNewListNotValid = true;
		});
    }
    
    // Add a task 
    // @param fdata object
    // @return void
    $scope.addTask = function(fdata){
	    var task = new taskFactory();
		task.task = fdata.target.value;
		task.list_id = $scope.activeListId;
		task.$save(
		function(taskRes){
			// Success
			$scope.defaultAddNewTaskNotValid = false;
			$scope.defaultAddNewTask = '';
			$scope.lists[taskRes.list_id].todo.push(taskRes);
		},
		function(){
			// Error
			$scope.defaultAddNewTaskNotValid = true;
		});
    }
    
    // Update a task (done or not done)
    // @param task object
    // @return void
    $scope.updateTask = function(task){
	    taskFactory.update({id:task.id}, {task:task.task, completed:task.completed},
		function(data){
			// Success
			if(task.completed==0)
			{
				// Put item back in todo array
				$scope.activeList.completed.splice($scope.activeList.completed.indexOf(task), 1);
				$scope.activeList.todo.push(task);
			}else if(task.completed==1){
				// Put item back in complete array
				$scope.activeList.todo.splice($scope.activeList.todo.indexOf(task), 1);
				$scope.activeList.completed.push(task);
			}
			// Update global lists array
			$scope.lists[$scope.activeListId].todo = $scope.activeList.todo;
			$scope.lists[$scope.activeListId].completed = $scope.activeList.completed;
		},
		function(){
			// Error
		});
    }
    
    // Remove a task
    // @param task object
    // @return void
    $scope.removeTask = function(task){
	    taskFactory.remove({id:task.id},
		function(data){
			// Success
			if(task.completed){
				$scope.activeList.completed.splice($scope.activeList.completed.indexOf(task), 1);
			}else{
				$scope.activeList.todo.splice($scope.activeList.todo.indexOf(task), 1);
			}
			
		},
		function(){
			// Error
		});
    }
    
    // Change the active list on click
    // @param listId integer
    // @return void
    $scope.changeToList = function(listId){
	    if(listId!==$scope.activeListId){
			$scope.activeListId = listId;
			listFactory.get({id:listId},
			function(data){
				// Success
				$scope.activeList = $scope.lists[$scope.activeListId];
		},
		function(){
			// Error
		});
		}
    }
    
    // Init the application
    init();
    
    // Validators
    $scope.validateText = function(text){
		var patt = new RegExp("[a-z A-Z]+");
		var res = patt.test(text);
	    if(!res){
		    return 'Not a valid name';
	    }
    }
        
}
// Against minifying we inject the variables here
HomeController.$inject = ['$scope', 'listFactory', 'taskFactory', '$cacheFactory', '$log'];
angular.module('todoApp')
    .controller('HomeController', HomeController);
}());
