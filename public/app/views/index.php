<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ToDo - Just do it!</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Stylesheets -->
		<link rel="stylesheet" type="text/css" href="../assets/css/reset.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/fonts.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/xeditable.css">
		<link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
    </head>
    <body ng-app='todoApp' ng-cloak>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Content -->
        <div ng-view></div>
		
		<!-- Angular files -->
        <script type="text/javascript" src="app/vendor/google/angular.min.js"></script>
		<script type="text/javascript" src="app/vendor/google/angular-route.min.js"></script>
		<script type="text/javascript" src="app/vendor/google/angular-sanitize.min.js"></script>
		<script type="text/javascript" src="app/vendor/google/angular-resource.min.js"></script>
		<script type="text/javascript" src="app/vendor/google/ui-utils.min.js"></script>
		<script type="text/javascript" src="app/vendor/google/angular-animate.min.js"></script>
		<script type="text/javascript" src="app/vendor/xeditable/xeditable.min.js"></script>
		
		<!-- Angular application -->
        <script src="app/app.js"></script>
		
		<!-- Angular controllers -->
        <script src="app/controllers/homeController.js"></script>
        
        <!-- Angular factories -->
        <script src="app/services/listFactory.js"></script>
        <script src="app/services/taskFactory.js"></script>
    </body>
</html>
