/**
 * AngularJS file for ToDo app
 * Author: Stephen Bouckaert (stephenbouckaert.com)
 * Created: 10/12/2014
  */
(function(){
    var app = angular.module('todoApp', ['ngRoute', 'ngAnimate', 'ngSanitize', 'ngResource', 'xeditable', 'ui.keypress']);
    app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
        // Routes go here
         // Routes go here
        $routeProvider
            .when('/',{
                controller: 'HomeController',
                templateUrl: 'app/views/home.html'
            })
            .otherwise({ redirectTo: '/' });
			$locationProvider.html5Mode(true);
    }]);
    app.run(function(editableOptions, editableThemes) {
	  editableThemes.bs3.inputClass = 'input-sm';
	  editableThemes.bs3.buttonsClass = 'btn-sm';
	  editableOptions.theme = 'bs3';
	});
}());
