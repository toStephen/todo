<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Standard route to homepage -> AngularJS app
Route::get('/', function()
{
    return View::make('index');
});

// If route doesn't exist go to home -> AngularJS fallback link
App::missing(function($exception)
{
    return View::make('index');
});

// Api routes
Route::group(array('prefix' => 'api/v1'), function()
{
	Route::resource('lists', 'ListController', array('only' => array('index', 'show', 'update', 'destroy', 'store')));
    Route::resource('tasks', 'TaskController', array('only' => array('store', 'update', 'destroy')));
});
