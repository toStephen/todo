<?php

class ListController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		try{
			$statusCode = 200;
			$response = [
				'lists' => []
			];
			
			$lists = ListModel::with('tasks')->get();
			
			foreach($lists as $list)
			{
				$todo = [];
				$completed = [];
				
				foreach($list->tasks as $task){
					if($task->completed == 1){
						array_push($completed, $task);
					}else{
						array_push($todo, $task);
					}
				}
				
				$response['lists'][$list->id] = [
					'id'			=> $list->id,
					'name'			=> $list->name,	
					'created_at'	=> $list->created_at,	
					'updated_at'	=> $list->updated_at,
					'tasks'			=> $list->tasks,
					'completed'		=> $completed,
					'todo'			=> $todo,
				];
			}
		}catch(Exception $e){
			$statusCode = 400;
		}finally{
			return Response::json($response, $statusCode);
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$list = [];
		
		$validator = Validator::make(
		    Input::all(),
		    array(
		        'name' => 'required | alpha_spaces',
		    )
		);
		if($validator->fails())
		{
		    return Response::json($list, $statusCode);
		}
		
		try{
			$statusCode = 200;
			
			$listSave = new ListModel;
			$listSave->name = Input::get("name");
			$listSave->save();
			
			$list = [
				'id'			=> $listSave->id,
				'name'			=> $listSave->name,	
				'created_at'	=> $listSave->created_at,	
				'updated_at'	=> $listSave->updated_at,
				'tasks'			=> $listSave->tasks,
				'completed'		=> [],
				'todo'			=> [],
			];			
		}catch(Exception $e){
			$statusCode = 400;
		}finally{
			return Response::json($list, $statusCode);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$statusCode = 200;
			$response = [
				'list' => []
			];
			
			$list = ListModel::with('tasks')->find($id);
				
			// Separate all tasks in 2 arrays
			$todo = [];
			$completed = [];
			
			foreach($list->tasks as $task){
				if($task->completed == 1){
					array_push($completed, $task);
				}else{
					array_push($todo, $task);
				}
			}

			$response = [
				'id'			=> $list->id,
				'name'			=> $list->name,
				'tasks'			=> $list->tasks,
				'todo'			=> $todo,
				'completed'		=> $completed,	
				'created_at'	=> $list->created_at,	
				'updated_at'	=> $list->updated_at
			];
		}catch(Exception $e){
			$statusCode = 400;
		}finally{
			return Response::json($response, $statusCode);
		}
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$statusCode = 400;
		$validator = Validator::make(
		    Input::all(),
		    array(
		        'data' => 'required | alpha_spaces',
		    )
		);
		if($validator->fails())
		{
		    return Response::json("{}", $statusCode);
		}

		try{
			$statusCode = 200;
			
			$list = ListModel::find($id);
			$list->name = Input::get("data");
			$list->save();
			
			return 'test';
			
		}catch(Exception $e){
			$statusCode = 400;
		}finally{
			return Response::json('{}', $statusCode);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try{
			$statusCode = 200;
			$list = ListModel::find($id)->delete();
		}catch(Exception $e){
			$statusCode = 400;
		}finally{
			return Response::json("{}", $statusCode);
		}
	}


}
