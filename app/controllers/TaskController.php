<?php

class TaskController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$statusCode = 400;
		// Validate input
		$validator = Validator::make(
		    Input::all(),
		    array(
		        'task' => 'required | alpha_spaces',
		        'list_id' => 'required | integer'
		    )
		);
		if($validator->fails())
		{
		    return Response::json(json_encode($validator->messages()), $statusCode);
		}
		
		try{
			$statusCode = 200;
			
			$task = new TaskModel;
			$task->task = Input::get("task");
			$task->list_id = Input::get("list_id");
			$task->save();
			
		}catch(Exception $e){
			$statusCode = 400;
		}finally{
			return Response::json($task, $statusCode);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$task = [];
		$statusCode = 400;
		// Validate input
		$validator = Validator::make(
		    Input::all(),
		    array(
		        'completed' => 'required | integer'
		    )
		);
		if($validator->fails())
		{
		    return Response::json('{}', $statusCode);
		}
		
		try{
			$statusCode = 200;
			
			$task = TaskModel::find($id);
			$task->completed = Input::get("completed");
			$task->save();
			
		}catch(Exception $e){
			$statusCode = 400;
		}finally{
			return Response::json($task, $statusCode);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try{
			$statusCode = 200;
			$list = TaskModel::find($id)->delete();
		}catch(Exception $e){
			$statusCode = 400;
		}finally{
			return Response::json("{}", $statusCode);
		}
	}


}
