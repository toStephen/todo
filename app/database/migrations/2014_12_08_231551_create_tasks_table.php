<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table)
		{
			// Values
			$table->increments('id');
			$table->string('task');
			$table->boolean('completed')->default(false);
			$table->integer('list_id')->unsigned();
			$table->timestamps();

			// Foreign keys
			$table->foreign('list_id')
			      ->references('id')->on('lists')
			      ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop foreign keys
		Schema::table('tasks', function(Blueprint $table) {
            $table->dropForeign('tasks_list_id_foreign');
        });
        // Drop table
		Schema::drop('tasks');
	}

}
