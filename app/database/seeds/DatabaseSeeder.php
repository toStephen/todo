<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		
		// Tables to seed
		$this->call('ListTableSeeder');
		$this->call('TaskTableSeeder');
		
		$this->command->info('Tables seeded!');
	}

}
