<?php
class TaskTableSeeder extends Seeder {
    public function run()
    {
        DB::table('tasks')->delete();
 
        TaskModel::create(array(
            'id' 		=> 1,
            'task' 		=> 'Water',
            'list_id' 	=> 1
        ));
 
        TaskModel::create(array(
            'id' 		=> 2,
            'task' 		=> 'Beer',
            'list_id' 	=> 1
        ));
        
        TaskModel::create(array(
            'id' 		=> 3,
            'task' 		=> 'Bugg spray',
            'list_id' 	=> 1
        ));
        
        TaskModel::create(array(
            'id' 		=> 4,
            'task' 		=> 'Take out the trash',
            'list_id' 	=> 2
        ));
        
        TaskModel::create(array(
            'id' 		=> 5,
            'task' 		=> 'Let the dog out',
            'list_id' 	=> 2
        ));
    }
}