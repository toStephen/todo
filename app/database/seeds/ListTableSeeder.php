<?php
class ListTableSeeder extends Seeder {
    public function run()
    {
        DB::table('lists')->delete();
 
        ListModel::create(array(
            'id' 	=> 1,
            'name' 	=> 'Shoplist'
        ));
 
        ListModel::create(array(
            'id' 	=> 2,
            'name' 	=> 'Todo'
        ));
    }
}