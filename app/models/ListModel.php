<?php

class ListModel extends \Eloquent {
	protected $fillable = [];
	
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lists';
    
    /**
     * Relationships.
     */
    public function tasks(){
        return $this->hasMany('TaskModel', 'list_id', 'id');
    }

}