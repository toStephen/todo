<?php

class TaskModel extends \Eloquent {
	protected $fillable = [];
	
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tasks';
    
    /**
     * Relationships.
     */
    public function tasks(){
        return $this->belongsTo('ListModel', 'id', 'list_id');
    }
}